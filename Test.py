# Test functions class
class TEST:
    passed = []
    failed = []

    def FAIL(self, name):
        self.failed.append(name)

    def PASS(self, name):
        self.passed.append(name)

    def GET_RAPORT(self):
        print('++++----++++----++++----++++----++++----++++----++++')
        print()
        print('\u2714 NUMBER OF PASSED TESTS: ', len(self.passed))

        if len(self.failed) > 0:
            print('x NUMBER OF FAILED TESTS: ', len(self.failed))
            print('x FAILED TESTS: ')
            for i in self.failed:
                print('       ', i)
        else:
            print()
            print('\u2714 \u2714 \u2714 \u2714   ALL TEST PASSED!  \u2714 \u2714 \u2714 \u2714')
            print('\u2714 \u2714 YOU ARE AWESOME PROGRAMMER! \u2714 \u2714')

        print()
        print('++++----++++----++++----++++----++++----++++----++++')
