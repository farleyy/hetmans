from Board import *
from config import *
import string

boardSize = config.boardSize

def startGame():
	coordsOfFirstHetman = askForHetman()
	findSetRestOfHetmans(coordsOfFirstHetman['x'], coordsOfFirstHetman['y'])

def askForHetman():
	alphabet = string.ascii_lowercase
	shouldAsk = True

	while shouldAsk:
		position = input('Input position of first hetman (For example: a1, d6): ')
		x = -1
		y = -1

		try:
			x = alphabet.index(position[0]) + 1
		except ValueError:
			print('Error in read X in position')

		try:
			y = int(position[1])
		except ValueError:
			print('Error in read Y in position')

		if (0 < x < boardSize) & (0 < y < boardSize):
			return {'x': x, 'y': y}
		else:
			print()
			print('>>> You should input letter in range ', alphabet[0], '-', alphabet[boardSize - 1])
			print('>>> You should input number in range ', '0 - ', boardSize - 1)
			print('>>> For example: a1, d6')
			print()

def findSetRestOfHetmans(firstX, firstY):
	hetmansCounter = 1

	# while(hetmansCounter <= 8):
	gameBoard = Board(boardSize)
	gameBoard.setHetman(firstX, firstY)

	for x in range(1, boardSize + 1):
		for y in range(1, boardSize + 1):
			if not gameBoard.isCollistionAt(x, y):
				gameBoard.setHetman(x, y)
				hetmansCounter += 1

	if hetmansCounter >= 8:
		gameBoard.show()
		print('Znaleziono 8 hetmanów na planszy!')
	else:
		gameBoard.show()
		print('Nie znaleziono :(')
		print('Znaleziono jedynie: ', hetmansCounter, ' hetmanów')

startGame()
