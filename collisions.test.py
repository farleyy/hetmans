from Board import *
from config import *
from Test import *
import inspect

my_name = lambda: inspect.stack()[1][3]
size = config.boardSize
T = TEST()

def run_all_tests():
	horizontal_top()
	edge_horizontal_top()
	horizontal_down()
	edge_horizontal_down()
	vertical_left()
	edge_vertical_left()
	vertical_right()
	edge_vertical_right()
	top_left_slant()
	top_right_slant()
	down_left_slant()
	down_right_slant()
	itself()
	bug_test_1()
	bug_test_2()
	T.GET_RAPORT()

# Test for horizontal-top
def horizontal_top():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(4, 2) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

def edge_horizontal_top():
	gameBoard = Board(size)

	gameBoard.setHetman(1, 1)
	if gameBoard.isCollistionAt(8, 1) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

# Test for horizontal-down
def horizontal_down():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(4, 6) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

def edge_horizontal_down():
	gameBoard = Board(size)

	gameBoard.setHetman(1, 8)
	if gameBoard.isCollistionAt(8, 8) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

# Test for vertical-left
def vertical_left():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(1, 4) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

def edge_vertical_left():
	gameBoard = Board(size)

	gameBoard.setHetman(1, 1)
	if gameBoard.isCollistionAt(1, 8) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

# Test for vertical-right
def vertical_right():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(8, 4) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

def edge_vertical_right():
	gameBoard = Board(size)

	gameBoard.setHetman(8, 1)
	if gameBoard.isCollistionAt(8, 8) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

# Test for top-left slant
def top_left_slant():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(1, 1) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

# Test for top-right slant
def top_right_slant():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(7, 1) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

# Test for down-left slant
def down_left_slant():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(1, 7) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

# Test for down-right slant
def down_right_slant():
	gameBoard = Board(size)

	gameBoard.setHetman(4, 4)
	if gameBoard.isCollistionAt(8, 8) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

def itself():
	gameBoard = Board(size)

	gameBoard.setHetman(1, 1)
	if gameBoard.isCollistionAt(1, 1) == True:
		T.PASS(my_name())
		return

		T.FAIL(my_name())

def bug_test_1():
	gameBoard = Board(size)

	gameBoard.setHetman(3, 3)
	if gameBoard.isCollistionAt(2, 4) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

def bug_test_2():
	gameBoard = Board(size)

	gameBoard.setHetman(2, 8)
	if gameBoard.isCollistionAt(5, 5) == True:
		T.PASS(my_name())
		return

	T.FAIL(my_name())

run_all_tests()
