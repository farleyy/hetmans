class Board:
	def __init__(self, size):
		self.board = []
		for x in range(0, size):
			self.board.append([])
			for y in range(0, size):
				self.board[x].append(0)

	def show(self):
		print()
		for i in self.board:
			print(i)
		print()

	def setHetman(self, x, y):
		self.board[y - 1][x - 1] = 1

	def isCollistionAt(self, xToCheck, yToCheck):
		board = self.board
		xToCheck -= 1
		yToCheck -= 1

		# itself
		if board[yToCheck][xToCheck] == 1:
			return True

		for i in range(0, len(board)):
			# vertical
			if board[i][xToCheck] == 1:
				return True

			# horizontal
			if board[yToCheck][i] == 1:
				return True

			# top-left slant
			if board[max(yToCheck - i, yToCheck - xToCheck)][max(xToCheck - i, 0)] == 1:
				return True

			# top-right slant
			if board[max(yToCheck - i, yToCheck - (7 - xToCheck), 0)][min(xToCheck + i, 7)] == 1:
				return True

			# down-right slant
			if board[min(yToCheck + i, 7)][min(xToCheck + i, 7)] == 1:
				return True

			# down-left slant
			if board[min(yToCheck + i, yToCheck + xToCheck , 7)][max(xToCheck - i, xToCheck - (7 - yToCheck) , 0)] == 1:
				return True

		return False
